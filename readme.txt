This code for this project was written in server-side javascript (Node.js).

If it isn't already installed and isn't in the package manager here are some instructions: http://blog.teamtreehouse.com/install-node-js-npm-linux

If it is in the package manager, just install it from there.

To run the project, cd to the code directory and type node <fileNameHere> into the terminal.

Files included are:
codes.js - works out codes
questionX.js - output for question X
utils.js - decode the data file
2-19-2-dataA.txt - supplied data file
decodedA.txt - decoded text, just to test the decoder works