'use strict';

var utils = require('./utils');
var codes = require('./codes');

var text = utils.decodeFile('2-19-2-dataA.txt');
var frequencies = codes.getFrequencies(text, 1);
var entropy = codes.getEntropy(frequencies);
var huffmanCode = codes.getHuffmanCodeBook(frequencies);
var huffmanExpectedLength = codes.getExpectedWordLength(frequencies, huffmanCode);
var shannonFanoCode = codes.getShannonFanoCodeBook(frequencies);
var shannonFanoExpectedLength = codes.getExpectedWordLength(frequencies, shannonFanoCode);

console.log('Symbol frequencies:');
console.log(utils.toSortedArray(frequencies));
console.log('Entropy:');
console.log(entropy);
console.log('Huffman code:');
console.log(utils.toSortedArray(huffmanCode));
console.log('Huffman code expected word length:');
console.log(huffmanExpectedLength);
console.log('Shannon-Fano code:');
console.log(utils.toSortedArray(shannonFanoCode));
console.log('Shannon-Fano code expected word length:');
console.log(shannonFanoExpectedLength);