'use strict';

var utils = require('./utils');
var codes = require('./codes');

var blockLength = 2;

var text = utils.decodeFile('2-19-2-dataA.txt');
var frequencies = codes.getFrequencies(text, blockLength);
var huffmanCode = codes.getHuffmanCodeBook(frequencies);
var huffmanExpectedLengthPerSymbol = codes.getExpectedWordLength(frequencies, huffmanCode)/blockLength;

console.log('Huffman code:');
console.log(utils.toSortedArray(huffmanCode));
console.log('Huffman code expected word length per symbol:');
console.log(huffmanExpectedLengthPerSymbol);