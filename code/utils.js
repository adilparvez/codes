'use strict';

var fs = require('fs');
var codes = require('./codes');

// Decode text file, <space> = 0, A = 1, ..., Z = 26.
function decodeFile(path) {
  // Build number code to character map.
  var map = {
    0: ' '
  };
  for (var i = 0; i < 26; ++i) {
    map[i + 1] = String.fromCharCode('A'.charCodeAt(0) + i);
  }
  // Read in file and decode it.
  try {
    var data = fs.readFileSync(path, 'utf8');
  } catch (err) {
    console.log(err);
  }
  var encoded = data.split(/[ \n\r]/).filter(function(value) {
    return value.length > 0
  });
  encoded = encoded.splice(0, encoded.length - 1);
  var decoded = [];
  for (var i = 0; i < encoded.length; ++i) {
    decoded.push(map[encoded[i]]);
  }
  return decoded.join('');
}

// Associative array to array of pairs sorted by key.
function toSortedArray(codeBook) {
  var sorted = [];
  for (var symbol in codeBook) {
    sorted.push([symbol, codeBook[symbol]]);
  }
  return sorted.sort();
}

exports.decodeFile = decodeFile;
exports.toSortedArray = toSortedArray;
