'use strict';

// Count the occurrences of symbol blocks in text.
// Pad text first so block length divides text length.
function getFrequencies(text, blockLength) {
  if (text.length % blockLength !== 0) {
    throw 'Must fit whole number of blocks in text.';
  } else {
    var frequencies = {};
    for (var i = 0; i < text.length; i += blockLength) {
      var symbolBlock = text.slice(i, i + blockLength);
      if (frequencies[symbolBlock]) {
        frequencies[symbolBlock]++;
      } else {
        frequencies[symbolBlock] = 1;
      }
    }
    return frequencies;
  }
}

// Calculate entropy from frequencies.
function getEntropy(frequencies) {
  var symbolCount = 0;
  for (var symbol in frequencies) {
    symbolCount += frequencies[symbol];
  }
  var entropy = 0;
  for (var symbol in frequencies) {
    var probability = frequencies[symbol]/symbolCount;
    entropy += probability * Math.log2(probability);
  }
  return -entropy;
}

// Frequency symbol pairs sorted by frequency.
function getPairs(frequencies) {
  var pairs = [];
  for (var symbol in frequencies) {
    pairs.push([frequencies[symbol], symbol]);
  }
  return pairs.sort(function(a, b) {
    return a[0] - b[0];
  });
}

// Build a Huffman tree.
function getHuffmanTree(pairs) {
  while (pairs.length > 1) {
    // Take the two symbols with smallest frequencies.
    var smallest = pairs.slice(0, 2);
    var others = pairs.slice(2);
    // Combine them.
    var combinedFrequency = smallest[0][0] + smallest[1][0];
    var combinedPair = [[combinedFrequency].concat([smallest])];
    pairs = others.concat(combinedPair);
    pairs.sort(function(a, b) {
      return a[0] - b[0];
    });
  }
  // Remove information about frequencies.
  var removeFrequencies = function(pairs) {
    var node = pairs[1];
    if (typeof node === 'string') {
      return node;
    } else {
      return [removeFrequencies(node[0]), removeFrequencies(node[1])];
    }
  }
  return removeFrequencies(combinedPair[0]);
}


// Build code book for Huffman code.
function getHuffmanCodeBook(frequencies) {
  var tree = getHuffmanTree(getPairs(frequencies));
  var codeBook = {};
  var getCode = function(node, codeWord) {
    if (typeof node === 'string') {
      codeBook[node] = codeWord;
    } else {
      getCode(node[0], codeWord + '0');
      getCode(node[1], codeWord + '1');
    }
  }
  getCode(tree, '');
  return codeBook;
}

// Get Shannon lengths, ceil(-log2(p)).
function getShannonLengths(frequencies) {
  var symbolCount = 0;
  for (var symbol in frequencies) {
    symbolCount += frequencies[symbol];
  }
  var lengths = {}
  for (var symbol in frequencies) {
    lengths[symbol] = Math.ceil(-Math.log2(frequencies[symbol]/symbolCount));
  }
  return lengths;
}

// Check if prefix is a prefix of string.
function isPrefix(prefix, string) {
  return string.indexOf(prefix) === 0;
}

// Get random bit string of certain length.
function getRandomBitString(length) {
  var bits = '';
  while (bits.length < length) {
    bits += (Math.random() < 0.5) ? 0 : 1;
  }
  return bits;
}

// Check if string prefixes or is prefixed by any value in a dictionary.
function prefixesOrPrefixedByAny(string, dictionary) {
  for (var key in dictionary) {
    if (isPrefix(string, dictionary[key]) || isPrefix(dictionary[key], string)) {
      return true;
    }
  }
  return false;
}

// Check if code is prefix free, for testing purposes.
function isPrefixFree(codeBook) {
  for (var i in codeBook) {
    for (var j in codeBook) {
      if (codeBook[i] !== codeBook[j] && (isPrefix(codeBook[i], codeBook[j]) || isPrefix(codeBook[j], codeBook[i]))) {
        return false;
      }
    }
  }
  return true;
}

// Build (random) code book for Shannon-Fano code.
function getShannonFanoCodeBook(frequencies) {
  var lengths = getShannonLengths(frequencies);
  var codeBook = {};
  for (var symbol in lengths) {
    while (true) {
      var codeWord = getRandomBitString(lengths[symbol]);
      if (!prefixesOrPrefixedByAny(codeWord, codeBook)) {
        codeBook[symbol] = codeWord;
        break;
      }
    }
  }
  return codeBook;
}

// Get expected length given symbol frequencies and code book.
function getExpectedWordLength(frequencies, codeBook) {
  var expectation = 0;
  var symbolCount = 0;
  for (var symbol in frequencies) {
    symbolCount += frequencies[symbol];
    expectation += frequencies[symbol]*(codeBook[symbol].length);
  }
  return expectation/symbolCount;
}

exports.getFrequencies = getFrequencies;
exports.getEntropy = getEntropy;
exports.getHuffmanCodeBook = getHuffmanCodeBook;
exports.isPrefixFree = isPrefixFree;
exports.getShannonFanoCodeBook = getShannonFanoCodeBook;
exports.getExpectedWordLength = getExpectedWordLength;
